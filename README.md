# Slim-MVC #
The project use Slim 3.0 with Eloquent & Twig

### Installation ###

* git clone https://mikechen01@bitbucket.org/coding_uncle/slim-mvc.git your-project-name
* cd your-project-name
* composer install
* chmod -R 777 storage/logs/
* chmod -R 777 storage/cahce/