<?php
return [
    'settings' => [
        'displayErrorDetails' => true, // set to false in production
        'addContentLengthHeader' => false, // Allow the web server to send the content-length header

        // Renderer settings
        'renderer' => [
            'template_path' => __DIR__ . '/../views/',
        ],

	    // View settings
        'view' => [
            'template_path' => __DIR__ . '/../views',
            'twig' => [
                'cache' => __DIR__ . '/../../storage/cache/twig',
                'debug' => APP_DEBUG_MODE,
                'auto_reload' => true,
            ],
        ],

        // Monolog settings
        'logger' => [
            'name' => 'slim-app',
            'path' => __DIR__ . '/../../storage/logs/app.log',
            'level' => \Monolog\Logger::DEBUG,
        ],

	    //Eloquent setting
	    'db' => [
	        'driver' => 'mysql',
	        'host' => 'localhost',
	        'database' => 'slim',
	        'username' => 'slimuser',
	        'password' => 'slimshady5566',
	        'charset' => 'utf8',
	        'collation' => 'utf8_unicode_ci',
	        'prefix' => '',
	    ],
    ],
];
