<?php
namespace App\Controllers;
use App\Models\User;

class HomeController extends Controller {
	public function index($request, $response) {
		$data['name'] = 'Mike';
		return $this->view->render($response, 'home.twig', $data);
	}

	public function users() {
		$users = User::all();
		// echo $users;
		return json_encode($users, JSON_UNESCAPED_UNICODE);
	}
}
