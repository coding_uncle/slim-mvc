<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;
/**
* 
*/
class User extends Model
{
	
	protected $table = 'fake_data';
	protected $fillable = [
		'email',
		'first_name',
		'last_name',
	];
	public function setPassword($password)
	{
		$this->update([
			'password' => password_hash($password,PASSWORD_DEFAULT)
		]);
	}
}
